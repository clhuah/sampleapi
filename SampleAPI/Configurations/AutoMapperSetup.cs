﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using SampleAPI.Application.AutoMapper;
using System;

namespace SampleAPI.Configurations
{
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            AutoMapperConfig.RegisterMappings();
        }
    }
}